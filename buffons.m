clear all

%adatok bekérése
prompt = {'Tű hossza: ','Rés hossza: ', 'Kisérletek száma:'};
dlgtitle = 'Adja meg a változók értékét';
definput = {'1','1','1000'};
opts.Interpreter = 'tex';
getInputValue1 = inputdlg(prompt,dlgtitle,[1 40],definput,opts);
%tű hossza
l = str2double(getInputValue1{1});
%rés hossza
d = str2double(getInputValue1{2});
%Kísérletek száma
N = str2double(getInputValue1{3});


Video = 0;
dn = 10; %lépésköz


Szog=zeros(N,1); %randomizált szögek listája
Tavolsag=zeros(N,1); %távolság
Logikai=zeros(N,1);t

%változók meghatározása
for i=1:N
    Szog(i,1)=rand(1,1)*(pi/2-0)+0;
    Tavolsag(i,1)=rand(1,1)*(d/2-0)+0;
    if((1/2)*sin(Szog(i)) >= Tavolsag(i))
        Logikai(i,1)=1;
    end
    
      
end

%kedvező esetek száma:
ni = numel(find(Logikai));
% Valószínűség kiszámítása
p = ni/N;


pi_kiszamitott = 2 * l / (d*p); % Pi közelítése
fprintf('"Pi" értékének meghatározása : %g\n',pi_kiszamitott);
fprintf('Relatív hiba : %+.1f%%\n',100*(pi_kiszamitott/pi-1));







% Ábra
Abra = figure('Color','c');



% Full screen
drawnow;
warning('off','all'); %kikapcsoljuk a zavaró warningot 
jFrame = get(Abra,'JavaFrame'); %%callbackek engedélyezése 
jFrame.setMaximized(true); %%fullscreen beállítása
warning('on','all'); %bekapcsoljuk a zavaró warningot 



% Pi értékek kiszámítása egyesével az ábrázoláshoz
Pi_ertekek = zeros(N,1);
for n = 1:N
    ni = numel(find(Logikai(1:n)));
    p = ni/n;
    Pi_ertekek(n) = 2 * l / (d*p);
end



%Szimuláció elkészítése dn lépésközzel
for n = 0:dn:N
    
    
    switch n
        
        %első lépés
        case 0
            
            % Tengelyek #1 
            Axes(1) = subplot(4,1,1:3);    %méretek beállítása        
            box('on');
            hold('on');
            title('Buffon féle tű kísérlet',...
                'Fontname','Times','FontAngle','Italic',...
                'Fontsize',20,'Fontweight','light');
            xlabel('Tű szöge [rad]');
            ylabel('Tű és a fa közötti távolság [cm]');

            %Intervallum vonal kiszámításához
            Angle0 = linspace(0,pi/2,N); 
            Distance0 = l/2*sin(Angle0);
            
         
            
            % Logical plots
            g = plot(nan,nan,'gx'); % Intervallumban
            r = plot(nan,nan,'rx'); % Intervallumon kívül
            
            % Határvonal
            plot(Angle0,Distance0,'b.');
            
            % Plot legend
            legend({'Kedvező eset','Nem kedvező eset','Határvonal'},'Fontsize',10);
            
            % 2. ábra         
            Axes(2) = subplot(4,1,4);                      
            box('on');
            hold('on');
            
            % Pi közelítése dn lépésközönként ábrázoláshoz
            Pi_abra = plot(1:dn,Pi_ertekek(1:dn),'black');
            
            % Pi
            Pi_line = plot(1:dn,pi*ones(1,dn),'r');               
            
            % Kettes ábra beállítás
            Legend = legend({sprintf('<\\Pi>'),sprintf('\\Pi')},...
                'Fontsize',10,'Location','NorthWest');          
            xlabel('Tű dobások száma');
            ylabel('Pi közelítése: \Pi');
            set(Axes(2),'Xscale','log');
            hold('on');
            grid('on');
            box('on');
            
         %ismétlés
        otherwise
                        
            % Pontok frissítése
            I = eq(Logikai(1:n),1);
            set(g,'Xdata',Szog(I),'Ydata',Tavolsag(I));
            set(r,'Xdata',Szog(~I),'Ydata',Tavolsag(~I));
            
            % aktuális kedvező esetszám
            ni = numel(find(Logikai(1:n)));
            
            % aktuális valószínűség
            p = ni/n;
            
            % pi aktuális kiszámítása
            pi_becsles = 2*l/(d*p);            
            Pi_ertekek(n) = pi_becsles;
            
            % Update
            set(Pi_abra,'Xdata',1:n,'Ydata',Pi_ertekek(1:n));            
            set(Pi_line,'Xdata',1:n,'Ydata',pi*ones(n,1));            
            set(Legend,...
                'String',{sprintf('<\\Pi> = %.4f',pi_becsles),...
                          sprintf('\\Pi (\\Delta\\Pi = %+.1f%%)',100*(pi_becsles/pi-1))});
            
            % Frissítés
            drawnow();
        
            
            
    end
    
end



